# WeBooking


<details>
  <summary>Assignment:</summary>
  iOS Assignment - Collect
"Book a room" application with a single API endpoint to fetch data, based on a provided design.
Instructions

A small company has an internal system to handle their meeting rooms. They already have a working version for web (see design), but are now interested in making a native iOS application.

Write a simple client for iOS that handles the following user scenarios:

A user wants to be able to see all of the rooms available
A user wants to book a room if it has available spots

Deliver your assignment by sending us the link to your repository (on Github, Gitlab or Bitbucket, etc.).

Include a README file detailing your approach.

Spend a maximum of 3 hours on this assignment. If you run out of time, just describe what you would've done in the README.

Specs

Use your best judgment to convert the web design to a mobile friendly one
Fetch the meeting rooms thanks to the following RESTful API endpoint:

https://wetransfer.github.io/rooms.json

When a user books a room, call the following RESTful API endpoint:

https://wetransfer.github.io/bookRoom.json

The list of rooms should be available offline
Write your code as if it were production code

Design:

https://www.figma.com/file/SVCALDDXuK010oEEzo6Bn9/Book-A-Room?node-id=0%3A1
</details>

### Chosen Approach
- Opted to use SwiftUI for this project for two reasons:
  - The design is very standard and looks like it could be built easily and quickly in SwiftUI compared to UIKit
  - Desire to try more SwiftUI out, and writing in a more production way
- Build a core for the Mobile app
  - This core acted as the center for requesting and booking rooms
  - Opted for utilizing combine as I was planning to use SwiftUI
  - Due to using SwiftUI updated Booking model to be Identifiable
- Built UI for the App
  - Utilized SwiftUi
  - Core screen handles 3 states
    - Loading
    - Success
    - Failed
  - Loading View was just a simple ProgressView for this project
  - Failed view was not fully implemented, but offers a retry button due to limited time of the challenge
  - Success view utilizes a lazy grid for more adaptive layouts so it is suitable for all screens
  - Introduced library URLImage for handling of async downloading the images
  - Introduce URLImageStore for cache of the images to help with performance in the SwiftUI view
  - Broke the views down into smaller components for cleaner viewing (Not sure how right of an approach I took with this, Feel like I missed some SwiftUI goodness, rather than the level of passing of values down I did)

#### Complications
- Loading images in SwiftUI views, this caused a lot of issues, and performance, if you scroll to the bottom, and then tap the time, you can see the device struggle, due to the nature of using a LazyVStack in a ScrollView.
- Image aspect ratio is messed up, didn't want to spend to much time on this problem, but I defined the ratio, but couldn't get the portrait image to look correct given the ratio
- Battles with Combine, originally I want Booker to contain a published property `rooms` which would then be observed and call back the viewModel to inform the view. In practice this didn't want to work the way I wanted it to, and rolled back to using AnyPublisher as a return type.
- When tapping the Book button, it naturally made all the buttons changed due to no individual selection option, had to add logic around this, wish iOS 15 was out so I could have utilize selection


### Next Steps
- Currently this solution is missing the following criteria:
 - 'The list of rooms should be available offline'
   - Currently the URL Images are cached thanks to using URLImage library
   - Network request response is not cached, that would have been the next steps to handle it failing
   - Offline mode would then need creating for the UI
     - Remove or disable the "Book!" button
     - Indicator show it is in offline mode
 - 'Write your code as if it were production code' (The following of things that are not do production readiness)
   - Add missing unit tests for view model
   - Add Tidy up the SwiftUI view a lot, personally regretting using SwiftUI for this, was hoping for a easy time with combine and the states that follow, but ended up with a lot of complications a long the way. I think it'd be better to tackle this using UIKit and UICollectionView specifically. The little things that tripped me up have been dealt with a lot easier (or utilizing the new APIs in iOS 15)
   
