//
//  RoomBookerViewModelTests.swift
//  WeBookingTests
//
//  Created by Alex Trott on 08/06/2021.
//

import XCTest
import Combine
@testable import WeBooking


class RoomBookerViewModelTests: XCTestCase {

    private var sut: RoomBookerViewModel!
    private var mockBooker: MockBooker = MockBooker()

    private var cancellable: AnyCancellable?

    override func setUp() {
        sut = RoomBookerViewModel(booker: mockBooker)
    }

    func test_init() {

        // then
        XCTAssertEqual(sut.title, "Rooms")
        XCTAssertEqual(sut.description, "Odio nisi, lectus dis nulla. Ultrices maecenas vitae rutrum dolor ultricies donec risus sodales. Tempus quis et.")
    }
}

class MockBooker: BookerInterface {

    var didCallList: Bool = false
    var listCalledCounter: Int = 0
    var stubbedList: Rooms!
    func listRooms() -> AnyPublisher<Rooms, NetworkError> {
        didCallList = true
        listCalledCounter += 1
        return Just(stubbedList)
            .mapError { _ in NetworkError.unknown }
            .eraseToAnyPublisher()
    }

    var didCallBook: Bool = false
    var bookCalledCounter: Int = 0
    var stubbedBooking: Booking!
    var capturedRoom: Room!
    func bookRoom(_ room: Room) -> AnyPublisher<Booking, NetworkError> {
        didCallBook = true
        capturedRoom = room
        bookCalledCounter += 1
        return Just(stubbedBooking)
            .mapError { _ in NetworkError.unknown }
            .eraseToAnyPublisher()
    }
}
