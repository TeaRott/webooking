//
//  MockNetworking.swift
//  WeBookingTests
//
//  Created by Alex Trott on 08/06/2021.
//

import Foundation
import Combine
@testable import WeBooking

class MockNetworking: Networking {

    var stubData: Data!

    func dataTask<T:Decodable>(_ request: URLRequest) -> AnyPublisher<T, NetworkError> {
        return Just(stubData)
            .decode(type: T.self, decoder: JSONDecoder())
            .mapError { NetworkError.request(underlyingError: $0) }
            .eraseToAnyPublisher()
    }
}
