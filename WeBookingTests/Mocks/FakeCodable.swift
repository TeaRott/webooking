//
//  FakeCodable.swift
//  WeBookingTests
//
//  Created by Alex Trott on 08/06/2021.
//

import Foundation

import Foundation

struct FakeCodable: Codable {
    let id: String
}

extension FakeCodable {

    func toData() -> Data {
        try! JSONEncoder().encode(self)
    }
}
