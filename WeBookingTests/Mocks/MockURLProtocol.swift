//
//  MockURLProtocol.swift
//  WeBookingTests
//
//  Created by Alex Trott on 08/06/2021.
//

import Foundation
import XCTest

class MockURLResponder {
    static var statusCode: Int = 200
    static var httpVersion: String = "HTTP/1.1"
    static var headerFields: [String : String]?
    static var responseData: Data = Data()
}

class MockURLProtocol: URLProtocol {

    override class func canInit(with request: URLRequest) -> Bool {
        true
    }

    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        request
    }

    override func startLoading() {
        guard let client = client else { return }
        do {
            let response = try XCTUnwrap(
                HTTPURLResponse(
                    url: XCTUnwrap(request.url),
                    statusCode: MockURLResponder.statusCode,
                    httpVersion: MockURLResponder.httpVersion,
                    headerFields: MockURLResponder.headerFields
                )
            )
            client.urlProtocol(self, didReceive: response, cacheStoragePolicy: .notAllowed)
            client.urlProtocol(self, didLoad: MockURLResponder.responseData)
        } catch {
            client.urlProtocol(self, didFailWithError: error)
        }
        client.urlProtocolDidFinishLoading(self)
    }

    override func stopLoading() {
    }
}
