//
//  BookerTests.swift
//  WeBookingTests
//
//  Created by Alex Trott on 08/06/2021.
//

import XCTest
import Combine
@testable import WeBooking

class BookerTests: XCTestCase {

    private var sut: Booker!
    private var mockRoomBooker: MockRoomBooker = MockRoomBooker()

    private var cancellable: AnyCancellable?

    override func setUp() {
        super.setUp()
        sut = Booker(roomBooker: mockRoomBooker)
    }

    func test_listRooms() {

        // given
        var capturedRooms: Rooms?
        mockRoomBooker.stubbedList = Rooms(rooms: [])

        // expect
        let expectation = self.expectation(description: "Publisher completion")

        // when
        cancellable = sut.listRooms()
            .sink { _ in
                expectation.fulfill()
            } receiveValue: { captured in
                capturedRooms = captured
            }

        // then
        waitForExpectations(timeout: 3)
        XCTAssertTrue(mockRoomBooker.didCallList)
        XCTAssertEqual(mockRoomBooker.listCalledCounter, 1)
        XCTAssertNotNil(capturedRooms)
    }

    func test_bookRoom() {

        // given
        let room = Room(name: "room-id", spots: 123, thumbnail: "url")
        var capturedBooking: Booking?
        mockRoomBooker.stubbedBooking = Booking(success: true)

        // expect
        let expectation = self.expectation(description: "Publisher completion")

        // when
        cancellable = sut.bookRoom(room)
            .sink { _ in
                expectation.fulfill()
            } receiveValue: { captured in
                capturedBooking = captured
            }

        // then
        waitForExpectations(timeout: 3)
        XCTAssertTrue(mockRoomBooker.didCallBook)
        XCTAssertEqual(mockRoomBooker.bookCalledCounter, 1)
        XCTAssertEqual(mockRoomBooker.capturedId, "room-id")
        XCTAssertTrue(capturedBooking!.success)
    }
}

class MockRoomBooker: RoomBookerService {

    var didCallList: Bool = false
    var listCalledCounter: Int = 0
    var stubbedList: Rooms!
    func list() -> AnyPublisher<Rooms, NetworkError> {
        didCallList = true
        listCalledCounter += 1
        return Just(stubbedList)
            .mapError { _ in NetworkError.unknown }
            .eraseToAnyPublisher()
    }

    var didCallBook: Bool = false
    var bookCalledCounter: Int = 0
    var stubbedBooking: Booking!
    var capturedId: String!
    func book(_ id: String) -> AnyPublisher<Booking, NetworkError> {
        didCallBook = true
        capturedId = id
        bookCalledCounter += 1
        return Just(stubbedBooking)
            .mapError { _ in NetworkError.unknown }
            .eraseToAnyPublisher()
    }
}
