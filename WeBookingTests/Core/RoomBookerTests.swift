//
//  RoomBookerTests.swift
//  WeBookingTests
//
//  Created by Alex Trott on 08/06/2021.
//

import XCTest
import Combine
@testable import WeBooking

class RoomBookerTests: XCTestCase {

    private var sut: RoomBooker!
    private var mockNetworking: MockNetworking = MockNetworking()

    private var cancellable: AnyCancellable?

    override func setUp() {
        super.setUp()
        sut = RoomBooker(network: mockNetworking)
    }

    func test_list() {

        // given
        let rooms: [Room] = [Room(name: "name", spots: 1, thumbnail: "url")]
        var capturedRooms: Rooms?

        mockNetworking.stubData = Rooms(rooms: rooms).toData()


        // expect
        let expectation = self.expectation(description: "Publisher completion")

        // when
        cancellable = sut.list()
            .sink { _ in
                expectation.fulfill()
            } receiveValue: { captured in
                capturedRooms = captured
            }

        // then
        waitForExpectations(timeout: 3)
        XCTAssertEqual(capturedRooms?.rooms.count, 1)
    }

    func test_bookRoom() {

        // given
        var capturedBooking: Booking?
        mockNetworking.stubData = Booking(success: true).toData()

        // expect
        let expectation = self.expectation(description: "Publisher completion")


        // when
        cancellable = sut.book("room_id")
            .sink { _ in
                expectation.fulfill()
            } receiveValue: { captured in
                capturedBooking = captured
            }

        // then
        waitForExpectations(timeout: 3)
        XCTAssertTrue(capturedBooking!.success)
    }
}
