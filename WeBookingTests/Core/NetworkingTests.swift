//
//  NetworkingTests.swift
//  WeBookingTests
//
//  Created by Alex Trott on 08/06/2021.
//

import XCTest
import Combine
@testable import WeBooking

class NetworkingTests: XCTestCase {

    private var cancellable: AnyCancellable?

    private var sut: Network!

    override func setUp() {
        super.setUp()
        let config = URLSessionConfiguration.ephemeral
        config.protocolClasses = [MockURLProtocol.self]

        sut = Network(session: URLSession(configuration: config))
    }

    override func tearDown() {
        cancellable = nil
        sut = nil
        super.tearDown()
    }

    func test_dataTask_returns200_andData() {

        // given
        setupMockURLResponder(
            responseData: FakeCodable(id: "FakeCodable - Stub1").toData()
        )
        var capturedFakeCodable: FakeCodable?
        let request = URLRequest(url: URL(string: "https://fake.com/asd")!)

        // expect
        let expectation = self.expectation(description: "Publisher completion")

        // when
        let publisher: AnyPublisher<FakeCodable, NetworkError> = sut.dataTask(request)
        cancellable = publisher.sink { _ in
            expectation.fulfill()
        } receiveValue: { fake in
            capturedFakeCodable = fake
        }

        // then
        waitForExpectations(timeout: 3)
        XCTAssertEqual(capturedFakeCodable?.id, "FakeCodable - Stub1")
    }

    func test_dataTask_returns400() {

        // given
        setupMockURLResponder(statusCode: 400)
        let request = URLRequest(url: URL(string: "https://fake.com/asd")!)

        // expect
        let expectation = self.expectation(description: "Publisher completion")

        // when
        let publisher: AnyPublisher<FakeCodable, NetworkError> = sut.dataTask(request)

        cancellable = publisher.sink { _ in
            expectation.fulfill()
        } receiveValue: { fake in
            XCTFail("No values received")
        }

        // then
        waitForExpectations(timeout: 3)
    }
}

private extension NetworkingTests {

    func setupMockURLResponder(
        statusCode: Int = 200,
        httpVersion: String = "HTTP/1.1",
        headerFields: [String : String]? = nil,
        responseData: Data = Data()
    ) {
        MockURLResponder.statusCode = statusCode
        MockURLResponder.httpVersion = httpVersion
        MockURLResponder.headerFields = headerFields
        MockURLResponder.responseData = responseData
    }
}

