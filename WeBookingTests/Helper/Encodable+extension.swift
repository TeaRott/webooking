//
//  Encodable+extension.swift
//  WeBookingTests
//
//  Created by Alex Trott on 08/06/2021.
//

import Foundation

extension Encodable {

    func toData() -> Data {
        guard let data = try? JSONEncoder().encode(self) else { return Data() }
        return data
    }
}
