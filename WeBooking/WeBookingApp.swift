//
//  WeBookingApp.swift
//  WeBooking
//
//  Created by Alex Trott on 05/06/2021.
//

import SwiftUI
import URLImage
import URLImageStore

@main
struct WeBookingApp: App {

    let urlImageService = URLImageService(
        fileStore: URLImageFileStore(),
        inMemoryStore: URLImageInMemoryStore()
    )

    var body: some Scene {
        WindowGroup {
            RoomBookerView()
                .environment(\.urlImageService, urlImageService)
        }
    }
}
