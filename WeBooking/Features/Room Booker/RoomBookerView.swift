//
//  ContentView.swift
//  WeBooking
//
//  Created by Alex Trott on 05/06/2021.
//

import SwiftUI
import URLImage

struct RoomBookerView: View {

    @ObservedObject var viewModel: RoomBookerViewModel

    init(viewModel: RoomBookerViewModel = RoomBookerViewModel()) {
        self.viewModel = viewModel
    }

    var body: some View {
        NavigationView {
            switch viewModel.state {
                case .isLoading:
                    ProgressView()
                        .onAppear { viewModel.onIsLoading()}
                        .navigationTitle(viewModel.title)
                case .failed(let error):
                    VStack {
                        Text(error.localizedDescription)
                        Button("Retry") { viewModel.onIsLoading() }
                    }
                    .navigationTitle(viewModel.title)
                case .loaded(let rooms):
                    ScrollView(.vertical, showsIndicators: false) {
                        NavigationDescriptionView(description: viewModel.description)
                        LazyVGrid(columns: Layouts.default, spacing: 16) {
                            ForEach(rooms, id: \.id) { room in
                                BookRoomView(room: room, state: viewModel.bookingState) {
                                    viewModel.bookRoom(room)
                                }
                            }
                        }.padding()
                    }
                    .navigationTitle(viewModel.title)
            }
        }
    }
}

struct RoomBookerView_Previews: PreviewProvider {
    static var previews: some View {
        RoomBookerView()
    }
}
