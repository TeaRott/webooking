//
//  RoomBookerViewModel.swift
//  WeBooking
//
//  Created by Alex Trott on 06/06/2021.
//

import Foundation
import Combine

// MARK: State
enum State: Equatable {
    static func == (lhs: State, rhs: State) -> Bool {
        switch (lhs, rhs) {
        case (.isLoading, .isLoading):
            return true
        default:
            return false
        }
    }

    case isLoading
    case failed(Error)
    case loaded([Room])
}

// MARK: State
enum BookingState {
    case isLoading(Room)
    case dormant
}

class RoomBookerViewModel: ObservableObject {

    // MARK: Propterties
    @Published
    private(set) var state = State.isLoading

    @Published
    private(set) var bookingState: BookingState = .dormant

    var title: String = "Rooms"
    var description: String = "Odio nisi, lectus dis nulla. Ultrices maecenas vitae rutrum dolor ultricies donec risus sodales. Tempus quis et."

    private let booker: BookerInterface
    private var cancellable: AnyCancellable?

    init(booker: BookerInterface = Booker()) {
        self.booker = booker
    }

    func onIsLoading() {
        getAllMeetingRooms()
    }

    private func getAllMeetingRooms() {
        cancellable = booker.listRooms()
            .receive(on: DispatchQueue.main)
            .sink(
                receiveCompletion: { error in
                    print("Error: \(error)")
                },
                receiveValue: { self.state = State.loaded($0.rooms) }
            )
    }

    func bookRoom(_ room: Room) {
        bookingState = .isLoading(room)
        cancellable = booker.bookRoom(room)
            .receive(on: DispatchQueue.main)
            .sink(
                receiveCompletion: { error in
                    self.bookingState = .dormant
                },
                receiveValue: { booking in
                    print("booking \(booking)")
                }
            )
    }
}
