//
//  NavigationDescriptionView.swift
//  WeBooking
//
//  Created by Alex Trott on 08/06/2021.
//

import SwiftUI

struct NavigationDescriptionView: View {

    let description: String

    init(description: String) {
        self.description = description
    }

    var body: some View {
        Text(description)
            .fontWeight(.light)
            .foregroundColor(Color("ColorDustyGray"))
            .padding()
    }
}

struct NavigationDescriptionView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationDescriptionView(description: "Placeholder")
    }
}
