//
//  BookRoomView.swift
//  WeBooking
//
//  Created by Alex Trott on 05/06/2021.
//

import SwiftUI
import URLImage

struct BookRoomView: View {

    let room: Room
    let state: BookingState
    let action: () -> Void

    init(
        room: Room,
        state: BookingState,
        action: @escaping () -> Void
    ) {
        self.room = room
        self.state = state
        self.action = action
    }

    var body: some View {
        VStack {
            RoomImageView(path: room.thumbnail)
            HStack(spacing:16) {
                VStack(alignment: .leading) {
                    Text(room.name)
                        .fontWeight(.semibold)
                        .frame(maxWidth: .infinity, alignment: .leading)
                    Text("\(room.spots) spots remaining")
                        .foregroundColor(Color("ColorFlirt"))
                        .frame(maxWidth: .infinity, alignment: .leading)
                }
                .frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/)
                VStack {
                    BookButton(title: "Book!", requesting: isRequesting) {
                        action()
                    }
                }
            }
            .frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/)
        }
        .padding(.bottom)
    }

    var isRequesting: Bool {
        switch state {
        case .isLoading(let bookingRoom):
            return bookingRoom.id == room.id
        default:
            return false
        }
    }
}

struct BookRoomView_Previews: PreviewProvider {
    static var previews: some View {
        let room = Room(
            name: "aaaaaaa",
            spots: 4,
            thumbnail: "https://images.unsplash.com/photo-1571624436279-b272aff752b5?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1504&q=80"
        )
        BookRoomView(
            room: room,
            state: .dormant,
            action: { print("Actioned") }
        )
    }
}
