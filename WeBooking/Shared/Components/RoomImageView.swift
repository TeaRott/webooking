//
//  RoomImageView.swift
//  WeBooking
//
//  Created by Alex Trott on 08/06/2021.
//

import SwiftUI
import URLImage

struct RoomImageView: View {

    let url: URL

    init(path: String) {
        self.url = URL(string: path)!
    }


    var body: some View {
        URLImage(url) {
            Image("Placeholder")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .cornerRadius(11)
        } inProgress: { progress in
            Image("Placeholder")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .cornerRadius(11)
        } failure: { error, action in
            Image("Placeholder")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .cornerRadius(11)
        } content: { image in
            image
                .resizable()
                .aspectRatio(CGSize(width: 6, height: 4), contentMode: .fill)
                .cornerRadius(11)
        }
        .environment(
            \.urlImageOptions,
            URLImageOptions(fetchPolicy: .returnStoreElseLoad())
        )
    }
}

struct RoomImageView_Previews: PreviewProvider {
    static var previews: some View {
        RoomImageView(path: "")
    }
}
