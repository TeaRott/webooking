//
//  BookButton.swift
//  WeBooking
//
//  Created by Alex Trott on 05/06/2021.
//

import SwiftUI

struct BookButton: View {

    let title: String
    let action: () -> Void
    let requesting: Bool

    init(
        title: String,
        requesting: Bool,
        action: @escaping () -> Void
    ) {
        self.title = title
        self.requesting = requesting
        self.action = action
    }

    var body: some View {
        Button(
            action: {
                action()
            }, label: {
                if requesting {
                    ProgressView()
                        .progressViewStyle(CircularProgressViewStyle(tint: .white))
                } else {
                    Text(title)
                        .fontWeight(.semibold)
                }
            }
        )
        .foregroundColor(.white)
        .padding(.horizontal)
        .frame(minWidth: 90, idealWidth: 90, minHeight: 30, idealHeight: 30)
        .background(Color("ColorFlirt"))
        .cornerRadius(4)
    }
}

struct BookButton_Previews: PreviewProvider {
    static var previews: some View {
        BookButton(title: "Book!", requesting: true) {
            print("\(#function)")
        }
    }
}
