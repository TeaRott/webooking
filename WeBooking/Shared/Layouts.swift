//
//  Layouts.swift
//  WeBooking
//
//  Created by Alex Trott on 08/06/2021.
//

import SwiftUI

struct Layouts {
    static var `default`: [GridItem] {
        Array(
            repeating: .init(
                .adaptive(minimum: 240),
                spacing: 16,
                alignment: .center
            ),
            count: 1
        )
    }
}
