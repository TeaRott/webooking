//
//  Networking.swift
//  WeBooking
//
//  Created by Alex Trott on 06/06/2021.
//

import Foundation
import Combine

protocol Networking {
    func dataTask<T: Decodable>(_ request: URLRequest) -> AnyPublisher<T, NetworkError>
}

class Network: Networking {

    enum NetworkingError: Error {
      case invalidData
    }

    private let session: URLSession

    init(session: URLSession = URLSession.shared) {
        self.session = session
    }

    func dataTask<T: Decodable>(_ request: URLRequest) -> AnyPublisher<T, NetworkError> {
        return session.dataTaskPublisher(for: request)
            .mapError { NetworkError.request(underlyingError: $0) }
            .map { $0.data }
            .decode(type: T.self, decoder: JSONDecoder())
            .mapError { $0 as? NetworkError ?? .unableToDecode(underlyingError: $0) }
            .eraseToAnyPublisher()
    }
}
