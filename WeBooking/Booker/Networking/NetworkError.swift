//
//  NetworkError.swift
//  WeBooking
//
//  Created by Alex Trott on 06/06/2021.
//

import Foundation

enum NetworkError: Error {
    case request(underlyingError: Error)
    case unableToDecode(underlyingError: Error)
    case unknown
}
