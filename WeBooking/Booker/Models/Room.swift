//
//  Room.swift
//  WeBooking
//
//  Created by Alex Trott on 06/06/2021.
//

import Foundation

struct Rooms: Codable {
    let rooms: [Room]
}

struct Room: Codable {
    let name: String
    let spots: Int
    let thumbnail: String
}

extension Room: Identifiable {
    var id: String {
        return name
    }
}
