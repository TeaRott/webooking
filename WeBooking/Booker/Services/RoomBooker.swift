//
//  RoomBooker.swift
//  WeBooking
//
//  Created by Alex Trott on 06/06/2021.
//

import Foundation
import Combine

protocol RoomBookerService {
    func list() -> AnyPublisher<Rooms, NetworkError>
    func book(_ id: String) -> AnyPublisher<Booking, NetworkError>
}

class RoomBooker: RoomBookerService {

    private let network: Networking

    init(network: Networking = Network()) {
        self.network = network
    }

    func list() -> AnyPublisher<Rooms, NetworkError> {
        var urlRequest = URLRequest(url: URL(string: "https://wetransfer.github.io/rooms.json")!)
        urlRequest.httpMethod = "GET"
        return network.dataTask(urlRequest)
    }

    func book(_ id: String) -> AnyPublisher<Booking, NetworkError> {
        var urlRequest = URLRequest(url: URL(string: "https://wetransfer.github.io/bookRoom.json")!)
        urlRequest.httpMethod = "GET"
        return network.dataTask(urlRequest)
    }
}
