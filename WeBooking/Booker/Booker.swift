//
//  Booker.swift
//  WeBooking
//
//  Created by Alex Trott on 06/06/2021.
//

import Foundation
import Combine

protocol BookerInterface {
    func listRooms() -> AnyPublisher<Rooms, NetworkError>
    func bookRoom(_ room: Room) -> AnyPublisher<Booking, NetworkError>
}

class Booker: ObservableObject {

    // MARK: Repositories
    private let roomBooker: RoomBookerService

    init(
        roomBooker: RoomBookerService = RoomBooker()
    ) {
        self.roomBooker = roomBooker
    }
}

extension Booker: BookerInterface {

    func listRooms() -> AnyPublisher<Rooms, NetworkError> {
        roomBooker.list()
    }


    func bookRoom(_ room: Room) -> AnyPublisher<Booking, NetworkError> {
        roomBooker.book(room.id)
    }
}
